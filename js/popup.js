let healthEl = document.getElementById('health');
healthEl.innerHTML = 'Проверяем...';

let dateEl = document.getElementById('date');
dateEl.innerHTML = '';

chrome.runtime.sendMessage({'method':'reload'},function(response){

  if(response.all === true)
  {
    healthEl.innerHTML = 'Торговля CS:GO работает!';
    healthEl.style.color = "green";
  }
  else
  {
    let html = 'Торговля CS:GO не работает!<br>Исправьте следующие проблемы:<br>';
    if(response.steamAccountIsAuthorized === false)
    {
      html += '--Авторизуйтесь в стиме<br>';
    }
    if(response.steamAccountIsAuthorized === true
    && response.steamApiKeyIsRegistered === false)
    {
      html += '--Создайте апи ключ в стиме<br>';
    }
    if(response.steamAccountIsAuthorized === true
    && response.traderAccountIsAuthorized === false)
    {
      html += '--Авторизуйтесь в стим трейдере<br>';
    }
    if(response.traderAccountIsAuthorized === true
    && response.steamApiKeyIsSavedOnTraderAccount === false)
    {
      html += '--Сохраните апи ключ стима в стим трейдере<br>';
    }
    healthEl.innerHTML = html;
    healthEl.style.color = "red";
  }

  let date = new Date(parseInt(response.lastCheckTime));
  let formattedDate = date.toLocaleDateString('ru') + ' - ' + date.toLocaleTimeString('ru');
  dateEl.innerHTML = 'Последняя проверка: ' + formattedDate;
});