export default function ExtensionManager (offers) {
	this.offers = offers;
	init(this);
}

function init(self) {
	onInstalled(self);
	onUserSendAction(self);
	onUserReloginOnSites(self);
	modifyRefererHeaderForTradeOfferCreating(self);	
};

function onInstalled(self) {
	// On Extension Installed
	chrome.runtime.onInstalled.addListener(async () => {
		await self.offers.checkAllHealthAndReload()
	});
}

function onUserSendAction(self) {
	// On User Clicked Popup Action
	chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
	    if(message.method === 'reload'){
	        (async () => {
	            sendResponse(await self.offers.checkAllHealthAndReload());
	        })();
	    }
	    return true;
	});
}

function onUserReloginOnSites(self) {
	// On User
	chrome.cookies.onChanged.addListener(async (changeInfo) => {
	    //logged in Steam
	    if(changeInfo.cookie.name === 'steamLoginSecure'
	    && changeInfo.cause === 'explicit') {
	    	setTimeout(async function(){
	    		let res = await self.offers.checkAllHealthAndReload();
	    	}, 10000);
	    }
	    //logged in Trader
	    if(changeInfo.cookie.name === 'sid'
	    && changeInfo.cause === 'explicit') {
	    	setTimeout(async function(){
	    		await self.offers.checkTraderHealthAndReload();
	    	}, 10000);
	    }
	});
}

function modifyRefererHeaderForTradeOfferCreating(self) {
	//Steam requires Referer Header when creating tradeoffer
	chrome.webRequest.onBeforeSendHeaders.addListener((details) => {
	    let newRef = self.offers.currentPartnerTradeUrl;

	    let gotRef = false;
	    for(let n in details.requestHeaders){
	        gotRef = details.requestHeaders[n].name.toLowerCase() === "referer";
	        if(gotRef){
	            details.requestHeaders[n].value = newRef;
	            break;
	        }
	    }
	    if(!gotRef){
	        details.requestHeaders.push({name:"Referer",value:newRef});
	    }
	    return {requestHeaders:details.requestHeaders};
	},{
	    urls:["https://steamcommunity.com/tradeoffer/new/*"]
	},[
	    "requestHeaders",
	    "blocking",
	    "extraHeaders"
	]);
}